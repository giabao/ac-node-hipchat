[ ![Codeship Status for rbergman/ac-node-hipchat](https://codeship.io/projects/fed54b50-fbc0-0131-d6f8-5a73486b8860/status)](https://codeship.io/projects/29244)

What?
=====

A set of modules useful for building web-framework-specific support for Atlassian Connection add-ons for HipChat.  It
is developed for use by the [ac-koa-hipchat] library, but should be useful for building similar adapters for other
web frameworks, should that be desirable.

The main library source is written to be compatible with Node.js v0.10.x, but the tests rely on ES6 generators,
which means you must use Node.js v0.11.x to run the tests.  Using `nvm` to manage multiple Node.js versions is
recommended.

Running Tests
=============

If you don't have `nvm`, install it.  It's not strictly required, but will aid tremendously in running multiple
versions of Node.js.

```
$ curl https://raw.githubusercontent.com/creationix/nvm/v0.12.1/install.sh | bash
```

For more advanced help with `nvm`, see its [website](https://github.com/creationix/nvm).

If you need Node.js v0.11.x, install and select it.

```
$ nvm install v0.11.13
$ nvm use v0.11.13
```

Next, make sure you have this repository's dependencies installed.

```
$ cd ac-node-hipchat
$ npm install
```

Finally, run the full test suite.

```
$ npm test
```