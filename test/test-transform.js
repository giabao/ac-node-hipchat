var assert = require('assert');
var transform = require('..').transform;

describe('ac hipchat descriptor transform', function () {

  describe('descriptor with capabilities', function () {
    var options = {
      name: 'test-add-on',
      displayName: 'Test Add-on',
      description: 'A test add-on',
      version: '0.0.1',
      author: {
        name: 'Atlassian',
        url: 'http://atlassian.com'
      }
    };
    var urls = {
      base: 'http://localhost:3000',
      homepage: '/',
      descriptor: '/addon/capabilities',
      installable: '/addon/installable'
    };

    it('should populate defaults from options and urls', function () {
      var descriptor = {
        capabilities: {}
      };
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          }
        }
      });
    });

    it('should make path urls full urls', function () {
      var descriptor = {
        links: {
          self: '/addon/capabilities',
          homepage: '/'
        },
        capabilities: {
          installable: {
            callbackUrl: '/addon/installable'
          },
          webhook: [{
            event: 'room_enter',
            url: '/addon/webhook'
          }],
          configurable: {
            url: '/configure'
          }
        }
      };
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          },
          webhook: [{
            event: 'room_enter',
            name: '90183e0516adf37661caa9efdb79c8d1472f992a',
            url: 'http://localhost:3000/addon/webhook?name=90183e0516adf37661caa9efdb79c8d1472f992a'
          }],
          configurable: {
            url: 'http://localhost:3000/configure'
          }
        }
      });
    });

    it('should not overwrite explicit values', function () {
      var descriptor = {
        key: 'custom-key',
        name: 'Custom Name',
        description: 'A custom description',
        version: '1.1.1',
        vendor: {
          name: 'Custom Vendor',
          url: 'http://custom.com'
        },
        links: {
          self: 'http://custom.com/addon/capabilities',
          homepage: 'http://custom.com'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: ['send_notification']
          },
          installable: {
            allowGlobal: true,
            allowRoom: true,
            callbackUrl: 'http://custom.com/installable'
          },
          configurable: {
            url: 'http://custom.com/configure'
          }
        }
      };
      assert.deepEqual(transform(descriptor, options, urls), descriptor);
    });
  });

});
