var assert = require('assert');
var sinon = require('sinon');
var check = require('check-types');
var wh = require('..').webhooks;

describe('ac hipchat webhooks', function () {

  describe('normalize', function () {

    var baseUrl = 'http://test.com:1000/foo';
    var defaultMountPath = '/ac-webhook';

    it('should not accept a webhook definition with no event', function () {
      try {
        var webhook = wh.normalize({}, baseUrl, defaultMountPath);
      } catch (e) {
        assert.ok(e);
        return;
      }
      assert.fail('no error', 'error', 'equal');
    });

    it('should not accept a webhook definition with an unrecognized event', function () {
      try {
        var webhook = wh.normalize({
          event: 'fail'
        }, baseUrl, defaultMountPath);
      } catch (e) {
        assert.ok(e);
        return;
      }
      assert.fail('no error', 'error', 'equal');
    });

    it('should not accept a webhook definition with event=room_message lacking a pattern', function () {
      try {
        var webhook = wh.normalize({
          event: 'room_message'
        }, baseUrl, defaultMountPath);
      } catch (e) {
        assert.ok(e);
        return;
      }
      assert.fail('no error', 'error', 'equal');
    });

    it('should accept a webhook definition with event=room_message with a string pattern', function () {
      var webhook = wh.normalize({
        event: 'room_message',
        pattern: '(?i)^/foo(?:\s(.*)|$)',
        url: 'http://test.com:1000/foo/ac-webhook'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.pattern, '(?i)^/foo(?:\s(.*)|$)');
      assert.equal(webhook.url, 'http://test.com:1000/foo/ac-webhook?name=649cdac0ba07ecc9b42320ce23f55360223f04ae');
    });

    it('should accept a webhook definition with event=room_message with a regex pattern', function () {
      var webhook = wh.normalize({
        event: 'room_message',
        pattern: /^\/foo(?:\s(.*)|$)/i,
        url: 'http://test.com:1000/foo/ac-webhook'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.pattern, '(?i)^\\/foo(?:\\s(.*)|$)');
      assert.equal(webhook.url, 'http://test.com:1000/foo/ac-webhook?name=6f0ce5209ca96a4c726ff5830d56ce353b9131f5');
    });

    it('should accept a webhook definition with a full url', function () {
      var webhook = wh.normalize({
        event: 'room_enter',
        url: 'http://test.com:1000/foo/ac-webhook'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.url, 'http://test.com:1000/foo/ac-webhook?name=2c8bf398f560e208070d3860ed30c6fb7fa0c4f5');
    });

    it('should complete a webhook definition with a relative url', function () {
      var webhook = wh.normalize({
        event: 'room_enter',
        url: '/foo/ac-webhook'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.url, 'http://test.com:1000/foo/ac-webhook?name=2c8bf398f560e208070d3860ed30c6fb7fa0c4f5');
    });

    it('should complete a webhook definition with no url', function () {
      var webhook = wh.normalize({
        event: 'room_enter'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.url, 'http://test.com:1000/ac-webhook?name=47523e29d46514060efdc0becdfdf3879d90d12e');
    });

    it('should complete a webhook definition with a bad url by fixing it', function () {
      var webhook = wh.normalize({
        event: 'room_enter',
        url: 'https://wrong.com:2000/ac-webhook'
      }, baseUrl, defaultMountPath);
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.url, 'http://test.com:1000/ac-webhook?name=47523e29d46514060efdc0becdfdf3879d90d12e');
    });

    it('should complete a webhook definition with no name with a stable, generated name', function () {
      var webhook = wh.normalize({
        event: 'room_message',
        pattern: /^!foo\s+(.+)/
      }, baseUrl, defaultMountPath);
      assert.ok(check.string(webhook.name));
      var name = webhook.name;
      delete webhook.name;
      webhook = wh.normalize(webhook, baseUrl, defaultMountPath);
      assert.equal(name, webhook.name);
      delete webhook.name;
      webhook.pattern = /^!bar\s+(.+)/;
      webhook = wh.normalize(webhook, baseUrl, defaultMountPath);
      assert.ok(check.string(webhook.name));
      assert.notEqual(name, webhook.name);
    });

  });

  describe('parseUrl', function () {

    it('should return mountPath and fullPath url elements for a full url', function () {
      var parsed = wh.parseUrl('https://wrong.com:2000/addon/ac-webhook');
      assert.equal(parsed.fullPath, '/addon/ac-webhook');
      assert.equal(parsed.mountPath, '/addon/ac-webhook');
    });

    it('should return mountPath and fullPath url elements for a partial url', function () {
      var parsed = wh.parseUrl('/addon/ac-webhook');
      assert.equal(parsed.fullPath, '/addon/ac-webhook');
      assert.equal(parsed.mountPath, '/addon/ac-webhook');
    });

    it('should return mountPath and fullPath url elements for a url with a query string', function () {
      var parsed = wh.parseUrl('https://wrong.com:2000/addon/ac-webhook?foo=bar');
      assert.equal(parsed.fullPath, '/addon/ac-webhook?foo=bar');
      assert.equal(parsed.mountPath, '/addon/ac-webhook');
    });

  });

  describe('regExpToPattern', function () {

    it('should return a python-compatible regex string given a js regexp object', function () {
      var pattern = wh.regExpToPattern(/^\/foo(?:\s(.*)|$)/gi);
      assert.equal(pattern, '(?gi)^\\/foo(?:\\s(.*)|$)');
    });

  });

  describe('digest', function () {

    it('should digest a webhook definition', function () {
      var digest = wh.digest({
        event: 'room_message',
        pattern: /^\/foo(?:\s(.*)|$)/i,
        url: 'https://wrong.com:2000/ac-webhook'
      });
    });

  });

});
