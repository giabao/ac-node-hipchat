var assert = require('assert');
var decorator = require('..').decorator;
var fixtures = require('./fixtures');

var tenant = fixtures.load('tenant.json');

describe('ac hipchat decorator', function () {

  var decorate;
  var testLocalBaseUrl = 'http://example.com/hipchat';
  var testAuthToken = '1234acbd';
  var testService = {test: true};

  function testServicesFactory(tenant) {
    return {testService: testService};
  }

  describe('in production', function () {

    beforeEach(function *() {
      decorate = decorator('production', testLocalBaseUrl, testServicesFactory);
    });

    it('should return a decorate function for a given service factory, local base url, and node env', function *() {
      var decorate = decorator('production', testLocalBaseUrl, testServicesFactory);
      assert.equal(typeof decorate, 'function')
    });

    it('should return a populated decoration object given a tenant in production mode', function *() {
      var decorate = decorator('production', testLocalBaseUrl, testServicesFactory);
      var decoration = decorate(tenant, testAuthToken);
      var tenantBaseUrl = tenant.links.base;
      assert.deepEqual(decoration.testService, testService);
      assert.deepEqual(decoration.locals, {
        localBaseUrl: testLocalBaseUrl,
        tenantBaseUrl: tenantBaseUrl,
        tenantScriptUrl: tenantBaseUrl + '/atlassian-connect/all.js',
        tenantStylesheetUrl: tenantBaseUrl + '/atlassian-connect/all.css',
        authToken: testAuthToken
      });
    });

  });

  describe('not in production', function () {

    beforeEach(function *() {
      decorate = decorator('development', testLocalBaseUrl, testServicesFactory);
    });

    it('should return a populated decoration object given a tenant in non-production mode', function *() {
      var decoration = decorate(tenant, testAuthToken);
      var tenantBaseUrl = tenant.links.base;
      assert.deepEqual(decoration.testService, testService);
      assert.deepEqual(decoration.locals, {
        localBaseUrl: testLocalBaseUrl,
        tenantBaseUrl: tenantBaseUrl,
        tenantScriptUrl: tenantBaseUrl + '/atlassian-connect/all-debug.js',
        tenantStylesheetUrl: tenantBaseUrl + '/atlassian-connect/all-debug.css',
        authToken: testAuthToken
      });
    });

  });

});
