var assert = require('assert');
var whExtractors = require('..').webhookExtractors;

describe('ac hipchat webhook extractors', function () {

  it('should extract normalized entities from a room_enter webhook', function () {
    var ctx = whExtractors['room_enter']({
      webhook_id: 10,
      item: {
        room: {
          id: 1
        },
        sender: {
          name: 'user'
        }
      }
    });
    assert.equal(ctx.webhookId, 10);
    assert.ok(ctx.room);
    assert.ok(ctx.sender);
    assert.equal(ctx.room.id, 1);
    assert.equal(ctx.sender.name, 'user');
  });

  it('should extract normalized entities from a room_exit webhook', function () {
    var ctx = whExtractors['room_exit']({
      webhook_id: 10,
      item: {
        room: {
          id: 1
        },
        sender: {
          name: 'user'
        }
      }
    });
    assert.equal(ctx.webhookId, 10);
    assert.ok(ctx.room);
    assert.ok(ctx.sender);
    assert.equal(ctx.room.id, 1);
    assert.equal(ctx.sender.name, 'user');
  });

  it('should extract normalized entities from a room_message webhook', function () {
    var ctx = whExtractors['room_message']({
      webhook_id: 10,
      item: {
        room: {
          id: 1
        },
        message: {
          id: 2,
          from: {
            name: 'user'
          },
          message: 'hello user'
        }
      }
    });
    assert.equal(ctx.webhookId, 10);
    assert.ok(ctx.room);
    assert.ok(ctx.sender);
    assert.ok(ctx.message);
    assert.equal(ctx.room.id, 1);
    assert.equal(ctx.sender.name, 'user');
    assert.equal(ctx.message.id, 2);
    assert.equal(ctx.content, 'hello user');
  });

  it('should extract normalized entities from a room_notification webhook', function () {
    var ctx = whExtractors['room_notification']({
      webhook_id: 10,
      item: {
        room: {
          id: 1
        },
        message: {
          id: 2,
          from: 'addon',
          message: 'hello addon'
        }
      }
    });
    assert.equal(ctx.webhookId, 10);
    assert.ok(ctx.room);
    assert.ok(ctx.sender);
    assert.ok(ctx.message);
    assert.equal(ctx.room.id, 1);
    assert.equal(ctx.sender.name, 'addon');
    assert.equal(ctx.message.id, 2);
    assert.equal(ctx.content, 'hello addon');
  });

  it('should extract normalized entities from a room_topic_change webhook', function () {
    var ctx = whExtractors['room_topic_change']({
      webhook_id: 10,
      item: {
        room: {
          id: 1
        },
        sender: {
          name: 'user'
        },
        topic: 'foo'
      }
    });
    assert.equal(ctx.webhookId, 10);
    assert.ok(ctx.room);
    assert.ok(ctx.sender);
    assert.equal(ctx.room.id, 1);
    assert.equal(ctx.sender.name, 'user');
    assert.equal(ctx.topic, 'foo');
  });

});
