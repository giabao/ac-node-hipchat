var assert = require('assert');
var MockHttpClient = require('./mock-http-client');
var MemoryStore = require('ac-node').MemoryStore;
var RestClient = require('..').RestClient;

describe('ac hipchat rest client', function () {

  var store = MemoryStore();
  var httpClient = MockHttpClient(10);

  afterEach(function *() {
    yield store.clear();
  });

  describe('stateless rest client', function () {

    var restClient = RestClient(httpClient);

    it('should fetch capabilities', function *() {
      var capabilitiesUrl = 'https://mock.hipchat.com/v2/capabilities';
      var capabilities = yield restClient.getCapabilities(capabilitiesUrl);
      assert.equal(capabilities.links.self, capabilitiesUrl);
    });

    it('should fetch an oauth token', function *() {
      var tokenUrl = 'https://mock.hipchat.com/v2/oauth/token';
      var username = 'foo';
      var password = 'bar';
      var scopes = [];
      var tokenData = yield restClient.generateToken(tokenUrl, username, password, scopes);
      assert.equal(tokenData.access_token, '1a2b3c4d5e6f');
    });

  });

});
