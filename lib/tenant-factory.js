var check = require('check-types');
var verify = check.verify;
var crypto = require('crypto');

module.exports = function (installable, capabilities) {
  check.map(installable, {
    oauthId: verify.string,
    oauthSecret: verify.string,
    groupId: verify.number,
    roomId: verify.maybe.number
  });

  check.map(capabilities, {
    links: {
      self: verify.webUrl,
      api: verify.webUrl,
      homepage: verify.webUrl
    },
    capabilities: {
      oauth2Provider: {
        tokenUrl: verify.webUrl
      }
    }
  });

  var webhookToken = crypto.createHash('sha1')
    .update(installable.oauthId)
    .update(installable.oauthSecret)
    .digest('hex');

  return {
    id: installable.oauthId,
    secret: installable.oauthSecret,
    group: installable.groupId,
    room: installable.roomId,
    webhookToken: webhookToken,
    links: {
      capabilities: capabilities.links.self,
      base: capabilities.links.homepage,
      api: capabilities.links.api,
      token: capabilities.capabilities.oauth2Provider.tokenUrl
    }
  };
};
